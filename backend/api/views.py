import json

from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache

import requests
from rest_framework.decorators import api_view

from backend.api.idarling_settings import *

# Serve Vue Application

index_view = never_cache(TemplateView.as_view(template_name='index.html'))

@api_view(['POST'])
def user_login(request):
    username = request.data.get('username', None)
    password = request.data.get('password', None)
    print("username: %s password: %s" % (username, password))
    user = auth.authenticate(username=username, password=password)
    if user:
        auth.login(request, user)
        return JsonResponse({"status": "ok"})
    else:
        return JsonResponse({"status": "failed"})

@api_view(['GET'])
def loginstatus(request):
    if request.user.is_authenticated:
        return JsonResponse({"status": "ok"})
    else:
        return JsonResponse({"status": "fail"})

@login_required
@api_view(['GET'])
def get_users(request):
    r = requests.get(IDARLING_GET_USERS)
    return JsonResponse(r.json())

@login_required
@api_view(['POST'])
def add_user(request):
    username = request.data["user"]
    password = request.data["password"]
    pubkey = request.data["pubkey"]

    r = requests.post(IDARLING_ADD_USER, json=
        {
            "user": username,
            "password": password,
            "pubkey": pubkey
        })
    return JsonResponse(r.json())

@login_required
@api_view(['POST'])
def del_user(request):
    r = requests.post(IDARLING_DEL_USER, json=
        {
            "user": request.data["user"]
        })
    return JsonResponse(r.json())

@login_required
@api_view(['POST'])
def update_user(request):
    username = request.data["user"]
    password = request.data["password"]
    pubkey = request.data["pubkey"]
    r = requests.post(IDARLING_UPDATE_USER, json=
        {
            "user": username,
            "password": password,
            "pubkey": pubkey,
        })
    return JsonResponse(r.json())

@login_required
@api_view(['POST'])
def list_tree(request):
    r = requests.post(IDARLING_LIST_TREE, json={"pathid": request.data["pathid"]})
    return JsonResponse(r.json())


@login_required
@api_view(['POST'])
def list_permission(request):
    r = requests.post(IDARLING_LIST_PERMISSION, json=
        {
            "user": request.data["user"],
            "nodeid": request.data["nodeid"],
            "permtype": request.data["permtype"],
        })
    return JsonResponse(r.json())

@login_required
@api_view(['POST'])
def add_permission(request):
    r = requests.post(IDARLING_ADD_PERMISSION, json=
        {
            "user": request.data["user"],
            "nodeid": request.data["nodeid"],
            "permtype": request.data["permtype"],
        })
    return JsonResponse(r.json())

@login_required
@api_view(['POST'])
def remove_permission(request):
    r = requests.post(IDARLING_REMOVE_PERMISSION, json=
        {
            "user": request.data["user"],
            "nodeid": request.data["nodeid"],
            "permtype": request.data["permtype"],
        })
    return JsonResponse(r.json())