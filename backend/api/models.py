from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models
from rest_framework import serializers

class IDArlingUser(AbstractBaseUser):
    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = []
    username = models.CharField(
        'username',
        max_length=150,
        primary_key=True,
    )