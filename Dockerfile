FROM nikolaik/python-nodejs:python3.6-nodejs12
ENV PYTHONUNBUFFERED 1

RUN mkdir /server
WORKDIR /server

RUN pip install -i https://pypi.tuna.tsinghua.edu.cn/simple pipenv

COPY Pipfile* /tmp/
RUN cd /tmp && pipenv lock --requirements > requirements.txt
RUN pip install -i https://pypi.tuna.tsinghua.edu.cn/simple -r /tmp/requirements.txt

COPY package.json *yarn* /server/
RUN yarn config set registry https://registry.npm.taobao.org/
RUN yarn install

COPY . /server/
RUN yarn build 
RUN python manage.py migrate

EXPOSE 8000

CMD python manage.py runserver 0.0.0.0:8000
