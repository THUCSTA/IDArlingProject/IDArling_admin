import request from '@/utils/request'

export function list_tree(pathid) {
  return request({
    url: '/projmanage/list_tree',
    method: 'post',
    data: {pathid: pathid}
  })
}